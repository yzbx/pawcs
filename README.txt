This directory contains a 'cleaned' version of the PAWCS method configuration as presented in
the 2015 WACV paper 'A Self-Adjusting Approach to Change Detection Based on Background Word Consensus'.

The main class used for background subtraction is BackgroundSubtractorPAWCS; all other files
contain either dependencies, utilities or interfaces for this method. It is based on OpenCV's
BackgroundSubtractor interface, and has been tested with version 3.0.0b. By default,
its constructor uses the parameters suggested in the paper.


TL;DR :

BackgroundSubtractorPAWCS bgs(...);
bgs.initialize(...);
for(all frames in the video) {
    ...
    bgs(input,output);
    ...
}


See LICENSE.txt for terms of use and contact information.
